CREATE TABLE t_Trener (
    trener_spk      INTEGER AUTO_INCREMENT PRIMARY KEY,
    imie            VARCHAR(30),
    nazwisko        VARCHAR(30)
);


CREATE TABLE t_Pokemon (
    pokemon_spk     INTEGER AUTO_INCREMENT PRIMARY KEY,
    trener_sfk      INTEGER,
    nazwa           VARCHAR(30),
    typ             VARCHAR(30),
    opis            VARCHAR(50),
    sila            INTEGER NOT NULL,
    szybkosc        INTEGER NOT NULL,
    posluszenstwo   INTEGER NOT NULL,
    doswiadczenie   INTEGER DEFAULT 0
);

ALTER TABLE t_Pokemon
    ADD FOREIGN KEY (trener_sfk)
    REFERENCES t_Trener(trener_spk)
    ON DELETE CASCADE;


CREATE TABLE t_PokemonWodnyWlasciwosci(
    pokemon_sfk                 INTEGER,
    szybkosc_w_wodzie           INTEGER,
    dlugosc_przezycia_na_ladzie INTEGER
);

ALTER TABLE t_PokemonWodnyWlasciwosci
    ADD FOREIGN KEY (pokemon_sfk)
    REFERENCES t_Pokemon(pokemon_spk)
    ON DELETE CASCADE;

CREATE TABLE t_PokemonOgnistyWlasciwosci(
    pokemon_sfk         INTEGER,
    szybkosc_w_ogniu    INTEGER
);

ALTER TABLE t_PokemonOgnistyWlasciwosci
    ADD FOREIGN KEY (pokemon_sfk)
    REFERENCES t_Pokemon(pokemon_spk)
    ON DELETE CASCADE;


CREATE TABLE t_PokemonZdolnosci(
    nazwa_zdolnosci         VARCHAR(30) NOT NULL,
    pokemon_sfk             INTEGER NOT NULL,
    stopien                 INTEGER NOT NULL
);

ALTER TABLE t_PokemonZdolnosci
    ADD FOREIGN KEY (pokemon_sfk)
    REFERENCES t_Pokemon(pokemon_spk)
    ON DELETE CASCADE;
