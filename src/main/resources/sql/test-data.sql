INSERT INTO t_Trener(imie, nazwisko) VALUES
    ('Marek', 'Kondziolka'),
    ('Jan', 'Kowalski'),
    ('Radoslaw', 'Kondziolka'),
    ('Michal', 'Kazek'),
    ('Kacper', 'Staw');

INSERT INTO t_Pokemon(trener_sfk, nazwa, typ, opis, sila, szybkosc, posluszenstwo) VALUES
    (1, 'Squirtle', 'WODNY', 'opis', 4 ,5, 6), -- 1
    (1, 'Seel', 'WODNY', 'opis',  7, 8, 9), -- 2
    (2, 'Horsea', 'WODNY', 'opis', 8,9,10), -- 3
    (5, 'Totodile', 'WODNY', 'opis', 2, 2,1), -- 4
    (4, 'Finneon', 'OGNISTY', 'opis', 12, 54, 76), -- 5
    (1, 'Chimchar', 'OGNISTY', 'opis', 12, 14, 25), -- 6
    (3, 'Tepig', 'OGNISTY', 'opis', 20, 30, 45), -- 7
    (3, 'Darumaka', 'OGNISTY', 'opis', 11, 14, 65), -- 8
    (4, 'Pansear', 'OGNISTY', 'opis', 43, 12, 32); -- 9

INSERT INTO t_PokemonWodnyWlasciwosci(pokemon_sfk, szybkosc_w_wodzie, dlugosc_przezycia_na_ladzie) VALUES
    (1, 10, 20);


INSERT INTO t_PokemonOgnistyWlasciwosci(pokemon_sfk, szybkosc_w_ogniu) VALUES
    (6, 15);

INSERT INTO t_PokemonZdolnosci(nazwa_zdolnosci, pokemon_sfk, stopien) VALUES
    ('uderzenie_kula_ognia', 7, 10),
    ('szał', 7, 20),
    ('szał', 9, 2),
    ('znikanie', 3, 0),
    ('hipnoza', 6, 18),
    ('znikanie', 4, 25),
    ('szał', 4, 15);
