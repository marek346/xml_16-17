package dao;

import exceptions.ServiceFaultException;
import exceptions.models.ServiceFault;
import lombok.val;
import mappers.PokemonMapper;
import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.mimuw.pokemon.FirePokemon;
import pl.edu.mimuw.pokemon.Pokemon;
import pl.edu.mimuw.pokemon.WaterPokemon;
import pl.edu.mimuw.pokemons.FightSide;
import pl.edu.mimuw.pokemons.GetPokemonsFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class PokemonDAO {

    @Autowired
    PokemonMapper pokemonMapper;

    @Autowired
    PersonDAO personDAO;

    @Transactional
    public Pokemon insertPokemon(Pokemon pokemon){

        personDAO.getPersons(new ArrayList<>(Arrays.asList(pokemon.getPersonId())));
        // here we are sure that there exists trainer for pokemon
        // contrary getPersons throws not found exception

        pokemonMapper.insertGeneralPokemon(pokemon);
        if (pokemon instanceof WaterPokemon){
            pokemonMapper.insertWaterPokemon((WaterPokemon) pokemon);
        }
        else if (pokemon instanceof FirePokemon){
            pokemonMapper.insertFirePokemon((FirePokemon) pokemon);
        }
        pokemonMapper.insertPokemonAbilities(pokemon);
        return pokemon;
    }

    @Transactional
    public void deletePokemons(List <Integer> pokemonIds){
        pokemonMapper.deletePokemons(pokemonIds);
    }


    public List<Pokemon> getPokemons(GetPokemonsFilter getPokemonsFilter){
        return pokemonMapper.getPokemons(getPokemonsFilter);

    }

    public List<Pokemon> getPokemons(List <Integer> pokemonIds){
        List <Pokemon> pokemons = pokemonMapper.getPokemonsById(pokemonIds);

        if (pokemons.size() == pokemonIds.size()) {
            return pokemons;
        }

        List<Integer> storedPokemonIds = (List<Integer>) CollectionUtils.collect(pokemons,
                new BeanToPropertyValueTransformer("pokemonId"));

        pokemonIds.removeAll(storedPokemonIds);

        throw new ServiceFaultException("ERROR",new ServiceFault(
                "NOT_FOUND", String.format("Pokemons with ids: %s not found.", pokemonIds.toString())));


    }

    @Transactional
    public void increaseExp(List<FightSide> fightSides){
        for (val fightSide : fightSides)
            pokemonMapper.increaseExp(fightSide);
    }

}
