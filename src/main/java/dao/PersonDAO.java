package dao;

import exceptions.ServiceFaultException;
import exceptions.models.ServiceFault;
import mappers.PersonMapper;
import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.mimuw.person.Person;
import pl.edu.mimuw.pokemons.GetPersonsFilter;

import java.util.List;

/**
 * Created by marek on 03.09.17.
 */
@Component
public class PersonDAO {

    @Autowired
    PersonMapper personMapper;

    public void deletePersons(List<Integer> personIds){
        personMapper.deletePersons(personIds);
    }

    public Person insertPerson(Person person){
        personMapper.insertPerson(person);
        return person;
    }

    public List<Person> getPersons(GetPersonsFilter filter){
        return personMapper.getPersons(filter);
    }

    public List <Person> getPersons(List <Integer> personIds){
        List <Person> persons = personMapper.getPersonsById(personIds);

        if (persons.size() == personIds.size()) {
            return persons;
        }

        List<Integer> storedPersonIds = (List<Integer>) CollectionUtils.collect(persons,
                new BeanToPropertyValueTransformer("personId"));

        personIds.removeAll(storedPersonIds);

        throw new ServiceFaultException("ERROR",new ServiceFault(
                "NOT_FOUND", String.format("Persons with ids: %s not found.", personIds.toString())));

    }

}
