package fights;

import dao.PokemonDAO;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.mimuw.pokemon.Pokemon;
import pl.edu.mimuw.pokemons.FightRequest;
import pl.edu.mimuw.pokemons.FightResult;
import pl.edu.mimuw.pokemons.FightSide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by marek on 03.09.17.
 */
@Component
public class FightSimulator {

    private final static int min = 10;
    private final static int max = 100;

    @Autowired
    private PokemonDAO pokemonDAO;

    private FightResult simulateFight(Pokemon pokemon1, Pokemon pokemon2){
        val expPokemon1 =  ThreadLocalRandom.current().nextInt(min, max + 1);
        val expPokemon2 =  ThreadLocalRandom.current().nextInt(min, max + 1);

        val winnerPokemon = ThreadLocalRandom.current().nextInt(0, 1 + 1) == 0 ? pokemon1 : pokemon2;
        val loserPokemon = winnerPokemon != pokemon1 ? pokemon1 : pokemon2;


        val loser = new FightSide();
        loser.setExp(expPokemon1);
        loser.setPokemonId(loserPokemon.getPokemonId());

        val winner = new FightSide();
        winner.setExp(expPokemon2);
        winner.setPokemonId(winnerPokemon.getPokemonId());

        val fightResult = new FightResult();
        fightResult.setLoser(loser);
        fightResult.setWinner(winner);

        pokemonDAO.increaseExp(Arrays.asList(loser, winner));

        return fightResult;
    }

    public FightResult simulateFight(FightRequest fightRequest){

        val pokemonId1 = fightRequest.getFight().getPokemonId1();
        val pokemonId2 = fightRequest.getFight().getPokemonId2();

        val pokemons = pokemonDAO.getPokemons(new ArrayList<>(Arrays.asList(pokemonId1, pokemonId2)));
        // here we are sure that pokemons.size() == 2, contrary getPokemons throw not found exception

        return simulateFight(pokemons.get(0), pokemons.get(1));

    }


}
