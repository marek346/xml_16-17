//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.09.04 at 12:13:08 PM CEST 
//


package pl.edu.mimuw.pokemons;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPokemonsFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPokemonsFilter"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="filterName" type="{http://www.mimuw.edu.pl/pokemons}FilterName" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="filterId" type="{http://www.mimuw.edu.pl/pokemons}FilterId" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="filterCoachName" type="{http://www.mimuw.edu.pl/pokemons}FilterCoachName" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPokemonsFilter", propOrder = {
    "filterName",
    "filterId",
    "filterCoachName"
})
public class GetPokemonsFilter {

    protected List<FilterName> filterName;
    protected List<FilterId> filterId;
    protected List<FilterCoachName> filterCoachName;

    /**
     * Gets the value of the filterName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the filterName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFilterName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FilterName }
     * 
     * 
     */
    public List<FilterName> getFilterName() {
        if (filterName == null) {
            filterName = new ArrayList<FilterName>();
        }
        return this.filterName;
    }

    /**
     * Gets the value of the filterId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the filterId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFilterId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FilterId }
     * 
     * 
     */
    public List<FilterId> getFilterId() {
        if (filterId == null) {
            filterId = new ArrayList<FilterId>();
        }
        return this.filterId;
    }

    /**
     * Gets the value of the filterCoachName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the filterCoachName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFilterCoachName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FilterCoachName }
     * 
     * 
     */
    public List<FilterCoachName> getFilterCoachName() {
        if (filterCoachName == null) {
            filterCoachName = new ArrayList<FilterCoachName>();
        }
        return this.filterCoachName;
    }

}
