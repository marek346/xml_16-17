package mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import pl.edu.mimuw.person.Person;
import pl.edu.mimuw.pokemons.GetPersonsFilter;

import java.util.List;

/**
 * Created by marek on 03.09.17.
 */
@Mapper
public interface PersonMapper {

    List<Person> getPersons(@Param("filter") GetPersonsFilter filter);

    void insertPerson(@Param("person") Person person);

    void deletePersons(@Param("personIds") List<Integer> personIds);

    List <Person> getPersonsById(@Param("personIds") List <Integer> personIds);
}
