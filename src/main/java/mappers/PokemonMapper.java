package mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import pl.edu.mimuw.pokemon.FirePokemon;
import pl.edu.mimuw.pokemon.Pokemon;
import pl.edu.mimuw.pokemon.WaterPokemon;
import pl.edu.mimuw.pokemons.FightSide;
import pl.edu.mimuw.pokemons.GetPokemonsFilter;

import java.util.List;

@Mapper
public interface PokemonMapper {

    List<Pokemon> getPokemons(@Param("filter") GetPokemonsFilter filter);

    void insertGeneralPokemon(@Param("pokemon") Pokemon pokemon);

    void insertFirePokemon(@Param("pokemon") FirePokemon pokemon);

    void insertWaterPokemon(@Param("pokemon") WaterPokemon pokemon);

    void insertPokemonAbilities(@Param("pokemon") Pokemon pokemon);

    void deletePokemons(@Param("pokemonIds") List <Integer> pokemonIds);

    List <Pokemon> getPokemonsById(@Param("pokemonIds") List <Integer> pokemonIds);

    void increaseExp(@Param("fightSide") FightSide fightSide);

}

