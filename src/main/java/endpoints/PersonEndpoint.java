package endpoints;

import configuration.WebServiceConfig;
import dao.PersonDAO;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.edu.mimuw.pokemons.DeletePersonsRequest;
import pl.edu.mimuw.pokemons.GetPersonsRequest;
import pl.edu.mimuw.pokemons.GetPersonsResponse;
import pl.edu.mimuw.pokemons.InsertPersonRequest;

import java.lang.reflect.Field;

/**
 * Created by marek on 03.09.17.
 */

@Endpoint
public class PersonEndpoint {

    @Autowired
    PersonDAO personDAO;

    @PayloadRoot(namespace = WebServiceConfig.NAMESPACE_URI, localPart = "deletePersonsRequest")
    @ResponsePayload
    public DeletePersonsRequest deletePersons(@RequestPayload DeletePersonsRequest deletePersons){
        personDAO.deletePersons(deletePersons.getPersonId());
        return deletePersons;
    }

    @PayloadRoot(namespace = WebServiceConfig.NAMESPACE_URI, localPart = "insertPersonRequest")
    @ResponsePayload
    public InsertPersonRequest insertPerson(@RequestPayload InsertPersonRequest insertPerson){
        personDAO.insertPerson(insertPerson.getPerson());
        return insertPerson;
    }

    @PayloadRoot(namespace = WebServiceConfig.NAMESPACE_URI, localPart = "getPersonsRequest")
    @ResponsePayload
    public GetPersonsResponse getPersons(@RequestPayload GetPersonsRequest getPersonsRequest) throws
            IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchFieldException{

        val persons = personDAO.getPersons(getPersonsRequest.getFilter());

        Class<?> clazz = Class.forName("pl.edu.mimuw.pokemons.GetPersonsResponse");
        Object cc = clazz.newInstance();

        Field f1 = cc.getClass().getDeclaredField("person");
        f1.setAccessible(true);
        f1.set(cc, persons);

        return (GetPersonsResponse) cc;
    }


}

