package endpoints;


import configuration.WebServiceConfig;
import dao.PokemonDAO;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.edu.mimuw.pokemons.DeletePokemonsRequest;
import pl.edu.mimuw.pokemons.GetPokemonsRequest;
import pl.edu.mimuw.pokemons.GetPokemonsResponse;
import pl.edu.mimuw.pokemons.InsertPokemonRequest;

import java.lang.reflect.Field;


@Endpoint
public class PokemonEndpoint {

    @Autowired
    PokemonDAO pokemonDAO;

    @PayloadRoot(namespace = WebServiceConfig.NAMESPACE_URI, localPart = "insertPokemonRequest")
    @ResponsePayload
    public InsertPokemonRequest insertPokemon(@RequestPayload InsertPokemonRequest insertPokemon) {
        pokemonDAO.insertPokemon(insertPokemon.getPokemon());
        return insertPokemon;
    }

    @PayloadRoot(namespace = WebServiceConfig.NAMESPACE_URI, localPart = "deletePokemonsRequest")
    @ResponsePayload
    public DeletePokemonsRequest deletePokemons(@RequestPayload DeletePokemonsRequest deletePokemons){
        pokemonDAO.deletePokemons(deletePokemons.getPokemonId());
        return deletePokemons;
    }

    @PayloadRoot(namespace = WebServiceConfig.NAMESPACE_URI, localPart = "getPokemonsRequest")
    @ResponsePayload
    public GetPokemonsResponse getPokemons(@RequestPayload GetPokemonsRequest getPokemonsRequest) throws
            IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchFieldException{

        val pokemons = pokemonDAO.getPokemons(getPokemonsRequest.getFilter());

        Class<?> clazz = Class.forName("pl.edu.mimuw.pokemons.GetPokemonsResponse");
        Object cc = clazz.newInstance();

        Field f1 = cc.getClass().getDeclaredField("pokemons");
        f1.setAccessible(true);
        f1.set(cc, pokemons);

        return (GetPokemonsResponse) cc;
    }

}
