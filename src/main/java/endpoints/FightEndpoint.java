package endpoints;

import configuration.WebServiceConfig;
import fights.FightSimulator;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.edu.mimuw.pokemons.FightRequest;
import pl.edu.mimuw.pokemons.FightResponse;

/**
 * Created by marek on 03.09.17.
 */

@Endpoint
public class FightEndpoint {

    @Autowired
    FightSimulator fightSimulator;

    @PayloadRoot(namespace = WebServiceConfig.NAMESPACE_URI, localPart = "fightRequest")
    @ResponsePayload
    public FightResponse fight(@RequestPayload FightRequest fightRequest){

        val fightResult = fightSimulator.simulateFight(fightRequest);

        val fightResponse = new FightResponse();
        fightResponse.setFightResult(fightResult);

        return fightResponse;

    }

}
