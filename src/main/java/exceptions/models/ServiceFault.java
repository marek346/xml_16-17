package exceptions.models; /**
 * Created by marek on 03.09.17.
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "exceptions.models.ServiceFault", propOrder = {
        "code",
        "description"
})
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ServiceFault {
    private String code;
    private String description;
}